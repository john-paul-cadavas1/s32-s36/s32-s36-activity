const Course = require("../models/Course");
const User = require("../models/User");

//Create a course - ADMIN Only
//Session 34 - Activity - Solution 1

module.exports.addCourse = (reqBody) => {

	let newCourse = new Course ({

		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price

	})

	return newCourse.save().then((course, error) => {

		if(error) {
			return false
		} else {
			return true
		}

	})
}
//Activity Solution 2
/*module.exports.addCourse = (reqBody, userData) => {

    return User.findById(userData.userId).then(result => {

        if (userData.isAdmin == false) {
            return "You are not an admin"
        } else {
            let newCourse = new Course({
                name: reqBody.name,
                description: reqBody.description,
                price: reqBody.price
            })
        
            //Saves the created object to the database
            return newCourse.save().then((course, error) => {
                //Course creation failed
                if(error) {
                    return false
                } else {
                    //course creation successful
                    return "Course creation successful"
                }
            })
        }
        
    });    
}
*/

//Retrieve all Courses

module.exports.getAllCourses = () => {
	return Course.find({}).then(result => {
		return result
	})
}

//Retrieve All Active Courses
module.exports.getAllActive = () => {

	return Course.find({isActive: true}).then(result => {
		return result
	})
}


//Retrieve a specific course
module.exports.getCourse = (reqParams) => {

	return Course.findById(reqParams.courseId).then(result => {
		return result;
	})
} 




//Updating a course
module.exports.updateCourse = (data) => {
	console.log(data)

	return Course.findById(data.courseId).then((result, error) => {

		console.log(result);

		if(data.isAdmin){
			result.name = data.updatedCourse.name
			result.description = data.updatedCourse.description
			result.price = data.updatedCourse.price

			console.log(result)

			return result.save().then((updatedCourse, error) => {

				if(error){
					return false
				} else {
					return updatedCourse
				}
			})

		} else {
			return false
		}
	})
}

// Session 35 - Activity - Solution
module.exports.archiveCourse = (data) => {

	return Course.findById(data.courseId).then((result, err) => {

		if(data.payload === true) {
			result.isActive = false;

			return result.save().then((archivedCourse, err) => {

				if(err) {

					return false;

				} else {

					return true;
				}
			})

		} else {

			//If user is not Admin
			return false
		}

	})
}

